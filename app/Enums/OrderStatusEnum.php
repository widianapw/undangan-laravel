<?php

namespace App\Enums;

use MadWeb\Enum\Enum;

/**
 * @method static OrderStatusEnum FOO()
 * @method static OrderStatusEnum BAR()
 * @method static OrderStatusEnum BAZ()
 */
final class OrderStatusEnum extends Enum
{
    const __default = self::WAITING_VERIFICATION;

    const WAITING_VERIFICATION = "0";
    const VERIFIED = "1";
    const SENT = "2";
    const NOT_VERIFIED = "3";

    public function getStatus($status){
        $text = "";
        if ($status == "0"){

            $text = "Menunggu Verifikasi";
        }
        return $text;
    }
}
