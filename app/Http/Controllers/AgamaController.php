<?php

namespace App\Http\Controllers;

use App\Agama;
use Illuminate\Http\Request;

class AgamaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Agama::with('sambutan')->get();
        return view("admin.agama.index", compact("data"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.agama.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Agama::insert($request->except(['_token']));
        return redirect()->route('admin.agama.index')->with('success', $this->SUCCESS_ADD_MESSAGE);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Agama $agama
     * @return \Illuminate\Http\Response
     */
    public function show(Agama $agama)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Agama $agama
     * @return \Illuminate\Http\Response
     */
    public function edit(Agama $agama)
    {
        return view("admin.agama.edit", compact("agama"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Agama $agama
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agama $agama)
    {
        Agama::find($agama->id)->update($request->all());
        return redirect()->route('admin.agama.index')->with('success', $this->SUCCESS_UPDATE_MESSAGE);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Agama $agama
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agama $agama)
    {
        Agama::find($agama->id)->delete();
        return redirect()->route('admin.agama.index')->with('success', $this->SUCCESS_DELETE_MESSAGE);
    }
}
