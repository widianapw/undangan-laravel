<?php

namespace App\Http\Controllers;

use App\Pesanan;
use App\Agama;
use App\Http\Requests\UndanganRequest;
use App\Images;
use Illuminate\Http\Request;
use Auth;
use App\Sambutan;
class CustomerController extends Controller
{
    public function index()
    {
        return view("customer.home");
    }

    public function register()
    {
        $agama = Agama::all();
        return view("customer.register",compact('agama'));
    }

    public function store(UndanganRequest $request)
    {

        $sambutan = Sambutan::where('agama_id',$request->agama)->first();
    
        // $data['photo'] = $request->file('gambar');
        // $data['gallery'] = $request->file('gallery');
        // dd($data);


        $phone_code = 62;
        $strwa = $request->whatsapp_number;
        $arr = array($phone_code,$strwa); 
        $wa = implode($arr);
        
        $tanggal = $request->date;
        $waktu = $request->time;
        
        $combinedDT = date('Y-m-d H:i:s', strtotime("$tanggal $waktu"));

        
	    $data = new Pesanan;
        $data->subdomain = $request->subdomain;
        $data->guy_nickname = $request->guy_nickname;
        $data->girl_nickname = $request->girl_nickname;
        $data->datetime_start = $combinedDT;
        $data->datetime_end = $combinedDT;
        $data->address = $request->address;
        $data->user_id = Auth::id();
        // $data->google_maps = $request->google_maps;
        $data->latitude = $request->latitude;
        $data->longitude = $request->longitude;
        $data->whatsapp_number = $wa;
        $data->sambutan_id = $sambutan->id;
        $data->harga_id = 1;
        if($request->hasfile('bukti_pembayaran'))
         {
            foreach($request->file('bukti_pembayaran') as $file)
            {
                $namePayment = time().'_'.$file->getClientOriginalName();
                $file->move(public_path().'/demos/payment/', $namePayment);  
                $hasil[] = $namePayment; 
                $data->payment_proof =$namePayment;
            }
         }
          
        $data->save();
        
        if($request->hasfile('gambar'))
         {
            foreach($request->file('gambar') as $file)
            {
                $name = time().'_'.$file->getClientOriginalName();
                $file->move(public_path().'/demos/foto/', $name);  
                $hasil[] = $name;
                $file= new Images();
                $file->undangan_id=$data->id;
                $file->image= $name;
                $file->status='0';
                $file->save();  
            }
         }

        if($request->hasfile('gallery'))
         {
            foreach($request->file('gallery') as $file)
            {
                $name = time().'_'.$file->getClientOriginalName();
                $file->move(public_path().'/demos/foto/', $name);  
                $hasil[] = $name; 
                $file= new Images();
                $file->undangan_id=$data->id;
                $file->image= $name;
                $file->status='1';
                $file->save(); 
            }
         }

         
    
	    return redirect('/')->with('success','Pembuatan berhasil dilakukan tunggu admin melakukan verifikasi terhadap bukti pembayaran');
 
    }


    public function undangan($domain)
    {
        $data = Pesanan::where('subdomain', $domain)->where('status', '2')->get();
        $undangan = Pesanan::where('subdomain', $domain)->first();
        // return $undangan;
        if ($data->count() == 0) {
            return redirect()->route("customer.home");
        } else {
            return view("customer.undangan", compact("undangan"));
        }

    }
}
