<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SambutanRequest extends FormRequest
{
    public function rules()
    {
        return [
            "pembuka_title" => "required|min:3",
            "penutup_title" => "required|min:3",
            "pembuka_message" => "required|min:10",
            "penutup_message" => "required|min:10",
        ];
    }

    public function authorize()
    {
        return true;
    }
}
