<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pesanan extends Model
{
    use SoftDeletes;

    protected $table = "undangans";
    protected $guarded = [];

    protected $with = ['sambutan', 'harga', 'images', 'user'];

    public function sambutan()
    {
        return $this->belongsTo(Sambutan::class, 'sambutan_id');
    }

    public function harga()
    {
        return $this->belongsTo(Harga::class, 'harga_id');
    }

    public function images()
    {
        return $this->hasMany(Images::class, 'undangan_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public static function getOrderStatus($status)
    {
        if ($status == "0") {
            return "Menunggu Verifikasi";
        } else if ($status == "1") {
            return "Terverifikasi";
        } else if ($status == "2") {
            return "Link Terkirim";
        } else if ($status == "3") {
            return "Selesai";
        } else {
            return "Tidak Terverifikasi";
        }
    }
}
