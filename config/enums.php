<?php

return [
    'order_status' => [
        '0' => 'Menunggu Verifikasi',
        '1' => 'Terverifikasi',
        '2' => 'Link Terkirim',
        '3' => 'Tidak Valid'
    ],
];
