var coordStr = [];
var poly;
var map, infoWindow;
var idJalan = $('#idJalan').val();
var data = [];
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
//maps
function initMap() {
    var geocoder, marker;
    var input = document.getElementById('search');
    var searchBox = new google.maps.places.SearchBox(input);
    geocoder = new google.maps.Geocoder();
    infoWindow = new google.maps.InfoWindow;
    var markers = [];
    map = new google.maps.Map(document.getElementById('mapKu'), {
        disableDefaultUI: false,
        styles: styles,
        center: { lat: -8.672716, lng: 115.226089 },
        zoom: 13,
        mapTypeControl: true,

    });

    marker = new google.maps.Marker({
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
    });

    infoWindow = new google.maps.InfoWindow;
    if (navigator.geolocation) {
        if ($('#update').is(':hidden')) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };

                infoWindow.setPosition(pos);
                infoWindow.setContent("<i class='fa fa-user'></i> Posisi anda saat ini");
                infoWindow.open(map);
                map.setZoom(15);
                map.setCenter(pos);

            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        }
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map.getCenter());
    }

    //search
    map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length === 0)
            return;

        markers.forEach(function(m) {
            m.setMap(null);
        });
        markers = [];

        var bounds = new google.maps.LatLngBounds();

        places.forEach(function(p) {
            if (!p.geometry)
                return;

            markers.push(new google.maps.Marker({
                map: map,
                title: p.name,
                position: p.geometry.location
            }));

            if (p.geometry.viewport)
                bounds.union(p.geometry.viewport);
            else
                bounds.extend(p.geometry.location);
        });
        map.fitBounds(bounds);
    });
    poly = new google.maps.Polyline({
        geodesic: true,
        strokeColor: '#FF0001',
        strokeOpacity: 1.0,
        strokeWeight: 5,
        geodesic: true
    });
    poly.setMap(map);
    poly.addListener('click', polyCLick);
    loadData();
}

function polyCLick(response) {
    infoWindow.setPosition(response.latLng);
    console.log(data)
    infoWindow.setContent()
    infoWindow.setContent("<div class='m-2 table-responsive'><b>" + data['nama_jalan'] + "</b><table><tbody><tr><td>Jenis Jalan</td><td>" + data['jenis_jalan']['jenis_jalan'] + "</td></tr><tr><td>Panjang Ruas</td><td>" + data['panjang_ruas'] + " meter" + "</td></tr><tr><td>Lebar Ruas</td><td>" + data['lebar_ruas'] + " meter" + "</td></tr></tbody></table>" +
        "<hr><button class='btn btn-primary btn-sm' data-toggle='modal' data-target='#myModal' id='streetView'> <i class='fas fa-street-view'></i> Street View</button></div>");
    infoWindow.open(map);
    map.setZoom(15);
    map.setCenter(response.latLng);
    $(document).on('click', '#streetView', function() {
        panoramaView(response.latLng);
    });
}

function loadData() {
    $.ajax({
        url: '/admin/validasi-digitasi-jalan/detail/' + idJalan,
        type: 'get',
        // data: { jalan: jalan, koordinat: coordStr, isEdit: true, id_jalan: $('#id_jalan').val() },
        success: function(response) {
            data = response;
            var coordinates = [];
            response['detail_digitasi'].forEach(function(item) {
                var pos = {}
                pos.lat = parseFloat(item.latitude);
                pos.lng = parseFloat(item.longitude);
                coordinates.push(pos);
            });
            setPolyline(coordinates);
            map.setCenter(coordinates[0]);
            map.setZoom(16);
            // location.href = "/digitasi-jalan";
        },
        error: function(e) {
            console.log(e);
        }
    });
}

function setPolyline(coordinates) {
    poly.setPath(coordinates);
    poly.setMap(map);
}

function panoramaView(myLatLng) {
    var panorama = new google.maps.StreetViewPanorama(
        document.getElementById('pano'), {
            pov: {
                heading: 240,
                pitch: 0
            },
            visible: true
        });
    panorama.setPosition(myLatLng);
}
var styles = [{
    "featureType": "poi",
    "stylers": [
        { "visibility": "off" }
    ]
}]