<?php

return[
    \App\Enums\OrderStatusEnum::class => [
        \App\Enums\OrderStatusEnum::WAITING_VERIFICATION => "Menunggu Verifikasi",
        \App\Enums\OrderStatusEnum::VERIFIED => "Terverifikasi",
        \App\Enums\OrderStatusEnum::SENT => "Link Terkirim",
        \App\Enums\OrderStatusEnum::NOT_VERIFIED => "Tidak Terverifikasi"
    ]
];
