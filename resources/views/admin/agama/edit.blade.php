@extends('layouts.layout')

@section('title','Edit Agama')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="card card-green">
                <div class="card-header">
                    <h3 class="card-title">Agama</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.agama.update',['agama'=>$agama->id])}}" method="POST" class="form-group">
                        @csrf
                        @method("PUT")
                        <div class="form-body">
                            <div class="form-label">
                                <label for="agama">Agama</label>
                            </div>
                            <input type="text" id="agana" name="agama" class="form-control" value="{{$agama->agama}}">

                            <br>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
