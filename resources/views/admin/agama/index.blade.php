@extends('layouts.layout')
@section('title','Agama')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="card card-green">
                <div class="card-header">
                    <h3 class="card-title">Agama</h3>
                </div>

                <div class="card-body ">
                    <a href="{{route('admin.agama.create')}}">
                        <button class="btn btn-primary mb-4"><i class="fa fa-plus"></i>
                            Tambah Data
                        </button>
                    </a>
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Agama</th>
                                <th>Tanggal Dibuat</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $m)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $m->agama }}</td>
                                    <td>@dateFormat($m->created_at)</td>
                                    <td>

                                        <a href="{{route('admin.agama.edit',['agama'=>$m->id])}}">
                                            <button type="submit" class="btn btn-warning">
                                                <i class="fa fa-fw fa-edit" style="color:white"></i>
                                            </button>
                                        </a>
                                    </td>
                                    <td>
                                        <form action="{{route('admin.agama.destroy',['agama'=>$m->id])}}" method="POST">
                                            @method("DELETE")
                                            @csrf
                                            <button type="submit" onclick="return confirm('Are you sure?')"
                                                    class="btn btn-danger">
                                                <i class="fa fa-fw fa-trash"></i>
                                            </button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#table').DataTable();
        });
    </script>
@endsection
