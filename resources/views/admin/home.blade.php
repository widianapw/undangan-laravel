@extends('layouts.layout')
@section('title','Admin')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">
            {{--            <div class="card card-blue">--}}
            {{--                <div class="card-header">--}}
            {{--                    <h3 class="card-title">Administrator</h3>--}}
            {{--                </div>--}}
            {{--                <div class="card-body text-center">--}}
            {{--                    <h4>Selamat Datang</h4>--}}
            {{--                    <p>Halo Admin</p>--}}
            {{--                    <br>--}}
            {{--                </div>--}}

            {{--            </div>--}}


            <div class="card card-purple">
                <div class="card-header">
                    <h3 class="card-title">Pendapatan Bulanan</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Bulan</th>
                                <th>Pendapatan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($pendapatanBulan as $m)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $m->bulan }}</td>
                                    <td>@currency($m->harga)</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#table').DataTable();
        });
    </script>
@endsection
