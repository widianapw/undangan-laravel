@extends('layouts.layout')
@section('title','Pesanan')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="card card-green">
                <div class="card-header">
                    <h3 class="card-title">Pesanan</h3>
                </div>

                <div class="card-body ">
                    <div class="table-responsive">
                        <table id="table" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>No.</th>
                                <th>Link</th>
                                <th>Mempelai Pria</th>
                                <th>Mempelai Wanita</th>
                                <th>Status</th>
                                <th>Tanggal Dibuat</th>
                                <th>Bukti Pembayaran</th>
                                <th>Ubah Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $m)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>widianapw.com/{{ $m->subdomain }}</td>
                                    <td>{{ $m->guy_nickname }}</td>
                                    <td>{{ $m->girl_nickname }}</td>
                                    <td>
                                        {{\App\Pesanan::getOrderStatus($m->status)}}
                                    </td>
                                    <td>@dateFormat($m->created_at)</td>
                                    <td>
                                        <center>
                                            <button id="{{asset('demos/payment/'.$m->payment_proof)}}"  
                    
                                                     class="btn btn-primary btn-modal">
                                                <i class="fa fa-eye"></i>
                                            </button>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                            <div class="btn-group">
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success dropdown-toggle"
                                                            data-toggle="dropdown">
                                                        Aksi
                                                    </button>
                                                    <div class="dropdown-menu">
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status-{{$m->id}}').value='1'; document.getElementById('changeStatusForm-{{$m->id}}').submit() ">Verifikasi</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status-{{$m->id}}').value='2'; document.getElementById('changeStatusForm-{{$m->id}}').submit() ">Link
                                                            Terkirim</a>
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status-{{$m->id}}').value='3'; document.getElementById('changeStatusForm-{{$m->id}}').submit() ">
                                                            Selesai
                                                        </a>
                                                        <a class="dropdown-item"
                                                           href="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                           onclick="event.preventDefault(); document.getElementById('status-{{$m->id}}').value='4'; document.getElementById('changeStatusForm-{{$m->id}}').submit() ">Tidak
                                                            Terverifikasi</a>

                                                        <form
                                                            id="changeStatusForm-{{$m->id}}"
                                                            action="{{route("admin.pesanan.changeStatus",["id"=>$m->id])}}"
                                                            method="POST" style="display: none">
                                                            @csrf
                                                            @method("PUT")
                                                            <input type="hidden" id="status-{{$m->id}}" name="status">
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </center>
                                    </td>

                                </tr>
                                
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModal" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Bukti Pembayaran</h5>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                    <img id="imgOnModal" class="img-fluid rounded mx-auto d-block "
                           >
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                        Tutup
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function () {
            $('#table').DataTable();
        });
        $('.btn-modal').on('click', function(e){
            var foto = $(this).attr('id');
            $('#exampleModal').modal('show');
            $('#imgOnModal').attr('src', foto);
        });
        
    </script>
@endsection
