@extends('layouts.layout')

@section('title','Tambah Sambutan')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="card card-green">
                <div class="card-header">
                    <h3 class="card-title">Sambutan</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.sambutan.store')}}" method="POST" class="form-group">
                        @csrf
                        <div class="form-body">
                            <div class="form-label">
                                <label for="agama">Agama</label>
                            </div>
                            <select name="agama_id" id="agama" class="form-control">
                                <option value="">Pilih Agama</option>
                                @foreach($agama as $i)
                                    <option value="{{$i->id}}">{{$i->agama}}</option>
                                @endforeach
                            </select>
                            <br>

                            <div class="form-label">
                                <label for="agama">Kata Pembuka</label>
                            </div>
                            <input type="text" name="pembuka_title" class="form-control">
                            <br>

                            <div class="form-label">
                                <label for="agama">Kata Penutup</label>
                            </div>
                            <input type="text" name="penutup_title" class="form-control">
                            <br>

                            <div class="form-label">
                                <label for="agama">Kalimat Pembuka</label>
                            </div>
                            <textarea name="pembuka_message" class="form-control" rows="6"></textarea>
                            <br>

                            <div class="form-label">
                                <label for="agama">Kalimat Penutup</label>
                            </div>
                            <textarea name="penutup_message" class="form-control" rows="6"></textarea>

                            <br>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary">Tambah</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
