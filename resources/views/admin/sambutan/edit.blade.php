@extends('layouts.layout')

@section('title','Edit Sambutan')
@section('content')
    <div class="row pt-2">
        <div class="col-md-12">
            <div class="card card-green">
                <div class="card-header">
                    <h3 class="card-title">Sambutan</h3>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.sambutan.update',['sambutan'=>$sambutan->id])}}" method="POST"
                          class="form-group">
                        @csrf
                        @method("PUT")
                        <div class="form-body">
                            <div class="form-label">
                                <label for="agama">Agama</label>
                            </div>
                            <select required name="agama_id" id="agama" class="form-control">
                                @foreach($agama as $i)
                                    @if($sambutan->agama_id == $i->id)
                                        <option selected value="{{$i->id}}">{{$i->agama}}</option>
                                    @else
                                        <option value="{{$i->id}}">{{$i->agama}}</option>
                                    @endif
                                @endforeach
                            </select>
                            <br>

                            <div class="form-label">
                                <label for="agama">Kata Pembuka</label>
                            </div>
                            <input type="text" value="{{$sambutan->pembuka_title}}" name="pembuka_title"
                                   class="form-control">
                            <br>

                            <div class="form-label">
                                <label for="agama">Kata Penutup</label>
                            </div>
                            <input type="text" value="{{$sambutan->penutup_title}}" name="penutup_title"
                                   class="form-control">
                            <br>

                            <div class="form-label">
                                <label for="agama">Kalimat Pembuka</label>
                            </div>
                            <textarea name="pembuka_message" class="form-control" rows="6">{{$sambutan->pembuka_message}}</textarea>
                            <br>

                            <div class="form-label">
                                <label for="agama">Kalimat Penutup</label>
                            </div>
                            <textarea name="penutup_message" class="form-control" rows="6">{{$sambutan->penutup_message}}</textarea>

                            <br>
                            <div class="form-footer">
                                <button type="submit" class="btn btn-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
