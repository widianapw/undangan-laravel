@extends('layouts.customer')
@section('title','Login Page')
@section('content')
<main>
        <!-- Contact form Start -->
        <div class="contact-form">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 offset-lg-3 offset-xl-3">
                        <div class="form-wrapper">
                             <!-- section tittle -->
                            <div class="row ">
                                <div class="col-lg-12">
                                    <div class="section-tittle tittle-form text-center">
                                        <h2>Login Page</h2>
                                    </div>
                                </div>
                            </div>
                            <form method="POST" action="{{ route('register') }}">
                                @csrf
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-box subject-icon mb-30">
                                            <input type="text" name="name" placeholder="Nama">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-box subject-icon mb-30">
                                            <input type="email" name="email" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-box subject-icon mb-30">
                                            <input type="password" name="password" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-box subject-icon mb-30">
                                            <input type="password" name="password_confirmation" placeholder="Konfirmasi Password">
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-12">
                                        
                                        <input type="submit" class="genric-btn info radius"  value="LOGIN">
                                        
                                    </div>
                                    </div>
                                </div>
                            </form>
                            <!-- Shape inner Flower -->
                            <div class="shape-inner-flower">
                                <img src="assets/img/flower/form-smoll-left.png" class="top1" alt="">
                                <img src="assets/img/flower/form-smoll-right.png" class="top2" alt="">
                                <img src="assets/img/flower/form-smoll-b-left.png"class="top3"  alt="">
                                <img src="assets/img/flower/form-smoll-b-right.png"class="top4"  alt="">
                            </div>
                            <!-- Shape outer Flower -->
                            <div class="shape-outer-flower">
                                <img src="assets/img/flower/from-top.png" class="outer-top" alt="">
                                <img src="assets/img/flower/from-bottom.png" class="outer-bottom" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Contact form End -->
    </main>

@endsection