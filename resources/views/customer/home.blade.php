@extends('layouts.customer')
@section('title','Beranda')
@section('content')
    <div class="slider-area ">
          @if (session('success'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Success!</h5>
                {{session('success')}}
            </div>
          @endif
          
            <!-- Mobile Menu -->
            <div class="single-slider slider-height2  hero-overly d-flex align-items-center" style="min-height: 600px" data-background="{{asset('customers/img/hero/about_hero.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8">
                          
                            <div class="hero-cap text-left">
                                <h2>Undangan nikah online gratis dengan ngantenYuks.</h2>
                                <p style="color: white">Buat dan bagikan undangan pernikahan kamu dengan berbagai pilihan tampilan undangan yang menarik dan pastinya gratis hanya di sini.</p>
                                <a href="{{route('customer.buatundangan')}}"><button class="genric-btn info circle">Buat Undangan Sekarang</button></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="whole-wrap" id="fitur">
            
            <div class="section-top-border">
                <div class="row">
                    <div class="col-md-2">
                        
                    </div>
                    <div class="col-md-8">
                        <div class="container box_1170">
                            <div class="text-center ">
                            <h1>FITUR APLIKASI</h1>
                            <p>Dengan fitur yang memudahkan, di Akanikah membuat undangan pernikahan online semudah menggeser mouse ditangan anda.</p>
                        </div>
                    </div>
                    <div class="col-md-2">
                       
                    </div>
                    
                </div>
                
            </div>
            </div>
                <div class="section-top-border">
                    <div class="row">
                        <div class="col-md-2">
                            
                        </div>
                        <div class="col-md-3">
                            <img src="customers/img/illust/cepat.png" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-5">
                            <h2>Mudah dan cepat</h2>
                            <p>Membuat undangan pernikahan online cepat, mudah dan pastinya gratis. Hanya beberapa menit dengan mengisi form registrasi maka undangan kamu akan siap untuk di bagikan keseluruh keluarga, sahabat dan kolegamu.
                                </p>
                        </div>
                        <div class="col-md-2">
                           
                        </div>
                        
                    </div>
                    
                   
                </div>
                <div class="section-top-border">
                    <div class="row" >
                        <div class="col-md-2">
                            
                        </div>
                        
                        <div class="col-md-5">
                            <h2>Subdomain unik</h2>
                            <p>Akanikah akan memberikan subdomain URL unik untuk website undangan kamu, dengan menggunakan kata-kata sesuai dengan keinginan kamu seperti akudandia.akanikah.com.
                                </p>
                        </div>
                        <div class="col-md-3">
                            <img src="customers/img/illust/website.png" alt="" class="img-fluid">
                        </div>
                        <div class="col-md-2">
                           
                        </div>
                        
                    </div>
                </div>
                <div class="section-top-border mt-4" >
                        <div class="row">
                            <div class="col-md-2">
                                
                            </div>
                            <div class="col-md-3">
                                <img src="customers/img/illust/media.png" alt="" class="img-fluid">
                            </div>
                            <div class="col-md-5">
                                <h2>Share dimana saja dan kapan saja</h2>
                                <p>Membuat undangan pernikahan online cepat, mudah dan pastinya gratis. Hanya beberapa menit dengan mengisi form registrasi maka undangan kamu akan siap untuk di bagikan keseluruh keluarga, sahabat dan kolegamu.
                                    </p>
                            </div>
                            <div class="col-md-2">
                               
                            </div>
                            
                        </div>
                </div>
                <div class="section-top-border mt-4" >
                    <div class="row">
                        <div class="col-md-4">
                            
                        </div>
                        <div class="col-md-4">
                            <a href="{{route('customer.buatundangan')}}"><button class="genric-btn info circle e-large btn-block">Buat Undangan Sekarang</button></a>
                        </div>
                        <div class="col-md-4">
                           
                        </div>
                        
                    </div>
                 </div>
                
               
            </div>
        </div>
@endsection
