@extends('layouts.customer')
@section('title','Registrasi ')
@section('content')

<div class="container mb-30">
    <div class="card">
    <div class="card-body">
        @if (count($errors)>0)
            <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-ban"></i> Validasi!</h5>
                @foreach ($errors->all() as $e)
                    {{$e}} 
                @endforeach
            </div>
        @endif
        <h5 class="card-title">Data Pasangan</h5>
        <form action="{{route('customer.tambahundangan')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
              <div class="col">
                <div class="mt-10">
                    <label for="">Nama Pria</label>
                    <input type="text" name="guy_nickname" placeholder="Nama Pria"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Pria'" required
                        class="single-input">
                </div>
              </div>
              <div class="col">
                <div class="mt-10">
                    <label for="">Nama Wanita</label>
                    <input type="text" name="girl_nickname" placeholder="Nama Wanita"
                        onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nama Wanita'" required
                        class="single-input">
                </div>
              </div>
              
            </div>
            <div class="row">
                <div class="col">
                    <div class="input-group-icon mt-10">
                        <div class="icon"><i class="fa fa-globe" aria-hidden="true"></i></div>
                        <div class="form-select" id="default-select">
                            <select name="agama">
                                <option value="">Pilih Agama</option>
                                @foreach ($agama as $key )
                                    <option value="{{$key->id}}">{{$key->agama}}</option>
                                @endforeach
                                
                            </select>
                        </div>
                    </div>
                </div>
                
              </div>
            <h5 class="card-title mt-4">Waktu dan Tempat Pernikahan</h5>
            <div class="row">
                <div class="col">
                    <div class="mt-10">
                        <label for="">Tanggal</label>
                        <input type="date" name="date" placeholder="Pilih Tanggal"
                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Pilih Tanggal'" required
                            class="single-input">
                    </div>
                </div>
               
                <div class="col">
                    <div class="mt-10">
                        <label for="">Waktu Mulai</label>
                        <input type="time" name="time" placeholder="Waktu Mulai"
                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Waktu Mulai'" required
                            class="single-input">
                    </div>
                  </div>
                  <div class="col">
                    <div class="mt-10">
                        <label for="">Tempat</label>
                        <input type="text" name="address" placeholder="Tempat Pernikahan"
                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Tempat Pernikahan'" required
                            class="single-input">
                    </div>
                  </div>
              </div>
              <div class="row mt-4">
                  <div class="col">
                    <div class="mt-10">
                        <label for="">Pilih Lokasi pada Maps ( Pindahkan Marker untuk Memilih Lokasi )</label>
                        {{-- <input type="text" name="google_maps" placeholder="Masukan link google maps"
                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Link google maps'" required
                            class="single-input"> --}}
                        <div id="mapid" style="height: 300px;"></div>
                        {{-- <input type="hidden" id="latitude" name="latitude" required>
                        <input type="hidden" id="longitude" name="longitude" required> --}}
                    </div>
                </div>
              </div>
              <div class="row mt-4">
                <div class="col">
                  <div class="mt-10">
                      <label for="">Latitude</label>
                      <input type="text" name="latitude" id="latitude" readonly class="single-input">
                  </div>
                </div>
                <div class="col">
                  <div class="mt-10">
                      <label for="">Longitude</label>
                      <input type="text" name="longitude" id="longitude"  readonly class="single-input">
                  </div>
                </div>
                
              </div>
              <div class="row mt-4">
                <div class="col">
                    <div class="mt-10">
                        <label for="basic-url">Nomor Telepon</label>
                            <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon3">+62</span>
                            </div>
                            <input required type="text" name="whatsapp_number" class="form-control" id="basic-url" aria-describedby="basic-addon3">
                            </div>
                        </div>
              </div>
              <div class="col">
                  <div class="mt-10">
                <label for="basic-url">Masukan Subdomain</label>
                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon3">https://ngantenyuk.widianapw.com/</span>
                    </div>
                    <input required type="text" class="form-control" name="subdomain" id="basic-url" aria-describedby="basic-addon3">
                    </div>
                </div>
              </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mt-10">
                        <label for="">Photo Cover</label>
                        <div class="custom-file">
                            <input required type="file" accept="image/x-png,image/gif,image/jpeg" name="gambar[]" class="custom-file-input" id="gambar">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                    </div>
                    
                </div>
                <div class="col">
                    <div class="mt-10">
                        <label for="">Gallery</label>
                        <div class="custom-file">
                            <input required type="file" accept="image/x-png,image/gif,image/jpeg" name="gallery[]" class="custom-file-input" id="gallery" multiple="multiple">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="mt-20">
                        <label for="">Bukti Pembayaran: <br><b>(Transfer Rp 100.000 ke rekening BNI 04893123123 atas nama Dewa Putu Pranata Swarimbawa)</b></label>
                        <div class="custom-file">
                            <input required type="file" accept="image/x-png,image/gif,image/jpeg" name="bukti_pembayaran[]" class="custom-file-input" id="gallery">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                          </div>
                    </div>
                </div>
            </div>
            <input type="submit" class="btn btn-primary mt-4" value="SUBMIT">
          </form>
        {{-- <a href="#" class="btn btn-primary">Go somewhere</a> --}}
    </div>
</div>
</div>
@endsection

@section('js')
    <script>
        var mymap = L.map('mapid').setView([-8.655924, 115.216934], 13);
        L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 20,
            id: 'mapbox/streets-v11',
            tileSize: 512,
            zoomOffset: -1,
            accessToken: 'pk.eyJ1Ijoid2lkaWFuYXB3IiwiYSI6ImNrNm95c2pydjFnbWczbHBibGNtMDNoZzMifQ.kHoE5-gMwNgEDCrJQ3fqkQ'
        }).addTo(mymap);
        marker = new L.marker([-8.655924, 115.216934], {draggable:'true'}).addTo(mymap);
        $('#latitude').val(-8.655924);
        $('#longitude').val(115.216934);
        marker.on('dragend', function(event){
            var marker = event.target;
            var position = marker.getLatLng();
            console.log(position);
            marker.setLatLng(new L.LatLng(position.lat, position.lng),{draggable:'true'});
            mymap.panTo(new L.LatLng(position.lat, position.lng))
            $('#latitude').val(position.lat);
            $('#longitude').val(position.lng);
        });
    </script>
    <script>
        // Add the following code if you want the name of the file appear on select
        $(".custom-file-input").on("change", function() {
          
          var fileName = $(this).val().split("\\").pop();
          var fileName1 = $(this).get(0).files.length;
          console.log(fileName1);
          if(fileName1>1){
            var text = fileName1 + " Gambar Terpilih";
            $(this).siblings(".custom-file-label").addClass("selected").html(text);
          }else{
          $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
          }
        });
        </script>

    
@endsection