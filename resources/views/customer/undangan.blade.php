<!DOCTYPE HTML>
<html lang="en">
<head>
    <title>My Wedding</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">


    <!-- Font -->

    <link href="https://fonts.googleapis.com/css?family=Playball%7CBitter" rel="stylesheet">

    <!-- Stylesheets -->

    <link href="{{asset('demos/common-css/bootstrap.css')}}" rel="stylesheet">


    <link href="{{asset('demos/common-css/fluidbox.min.css')}}" rel="stylesheet">

    <link href="{{asset('demos/common-css/font-icon.css')}}" rel="stylesheet">
    <link rel="icon" href="{{asset('demos/images/love.png')}}">

    <link href="{{asset('demos/01-homepage/css/styles.css')}}" rel="stylesheet">

    <link href="{{asset('demos/01-homepage/css/responsive.css')}}" rel="stylesheet">
    <link href="{{asset('demos/fontawesome/css/fontawesome.css')}}" rel="stylesheet">
    <link href="{{asset('demos/fontawesome/css/brands.css')}}" rel="stylesheet">
    <link href="{{asset('demos/fontawesome/css/solid.css')}}" rel="stylesheet">

</head>
<body>

<header>

    <div class="container">
        <input type="hidden" id="eventDate" value="@dateCountdown($undangan->datetime_start)"/>
        <a class="logo" href="#"><img src="{{asset('demos/images/logo-white.png')}}" alt="Logo"></a>

        <div class="menu-nav-icon" data-nav-menu="#main-menu"><i class="icon icon-bars"></i></div>

        <ul class="main-menu visible-on-click" id="main-menu">
            <li><a href="#HOME">HOME</a></li>
            <!-- <li class="drop-down"><a href="#!">OUR STORIES<i class="icon icon-caret-down"></i></a>

                <ul class="drop-down-menu">
                    <li><a href="#">FEATURED</a></li>
                    <li><a href="#">ABOUT</a></li>
                    <li class="drop-down"><a href="#!">CATEGORIES<i class="icon icon-caret-right"></i></a>
                        <ul class="drop-down-menu drop-down-inner">
                            <li><a href="#">FEATURED</a></li>
                            <li><a href="#">ABOUT</a></li>
                            <li><a href="#">CATEGORIES</a></li>
                        </ul>
                    </li>
                </ul>

            </li> -->

            <!-- <li><a href="03-regular-page.html">THER WEDDING</a></li> -->
            <li><a href="#undangan">UNDANGAN</a></li>
            <li><a href="#gallery">GALLERY</a></li>
            <li><a href="#contact">LOCATION MAPS</a></li>
            <li><a href="#ucapan">UCAPAN</a></li>
        </ul><!-- main-menu -->

    </div><!-- container -->
</header>


<div class="main-slider" id="home"
     style="background-image: url({{asset('demos/foto/'.$undangan->images->where('status','0')->first()->image)}}); box-shadow: 1px 10px 40px rgba(0,0,0,.4); ">
    <div class="display-table center-text">
        <div class="display-table-cell" style="padding-top: 100px; ">
            <div class="slider-content mb-3">

                <i class="small-icon icon icon-tie"></i>
                <h5 class="date">@dateFormat($undangan->datetime_start)</h5>
                <h3 class="pre-title">Save The Date</h3>
                <h1 class="title">{{$undangan->guy_nickname}} <i
                        class="icon icon-heart"></i> {{$undangan->girl_nickname}}</h1>
                <h4 class="pre-title">{{$undangan->address}}</h4>
                <!-- <h3 class="pre-title mt-2" style="color: pink"><a href="https://www.google.com/maps/place/Gg.+Melati+No.15,+Kesiman+Petilan,+Kec.+Denpasar+Tim.,+Kota+Denpasar,+Bali+80237/@-8.6521842,115.2457018,17z/data=!3m1!4b1!4m5!3m4!1s0x2dd24079e24dabb3:0x9a1cbfbb52eee3f9!8m2!3d-8.6521895!4d115.2478905">click for the address</a></h3> -->

                <!-- <div class="heading">
                    <h2 class="title">Don't Miss It!!!</h2>
                    <span class="heading-bottom"><i class="icon icon-star"></i></span>
                </div>

                 -->
            </div><!-- slider-content-->
        </div><!--display-table-cell-->

    </div><!-- display-table-->


</div><!-- main-slider -->
<section class="section story-area center-text" style="box-shadow: 1px 10px 40px rgba(0,0,0,.4);">
    <div class="container">
        <div class="heading">
            <h2 class="title">DON'T MISS IT!</h2>
            <span class="heading-bottom"><i class="icon icon-star"></i></span>
        </div>
        <div class="remaining-time mt-2">
            <div id="clock">

            </div>
        </div>
    </div>
</section>
<section class="section story-area center-text" id="undangan" style="box-shadow: 1px 10px 40px rgba(0,0,0,.4);">
    <div class="container">
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm-8">

                <div class="heading">
                    <h2 class="title">{{$undangan->sambutan->pembuka_title}}</h2>
                    <span class="heading-bottom"><i class="icon icon-star"></i></span>
                </div>

                <!-- <h3> Om Swastyastu </h3><br> -->
                <p style="text-indent: 60px; text-align: justify;">
                    {{$undangan->sambutan->pembuka_message}}
                    <br/><br>
                <center>
                    <div style="text-align: left">
                        <table>
                            <tr>
                                <td>Hari/Tanggal</td>
                                <td>: @dateFormat($undangan->datetime_start)</td>
                            </tr>
                            <tr>
                                <td>Pukul</td>
                                <td>: 10.00 WITA - Selesai</td>
                            </tr>
                            <tr>
                                <td>Alamat</td>
                                <td>: {{$undangan->address}}</td>
                            </tr>

                        </table>

                        <br/><br>
                    </div>
                </center>
                {{$undangan->sambutan->penutup_message}}
                <br>

                <h3>{{$undangan->sambutan->penutup_title}}</h3>
            </div><!-- col-sm-10 -->
            <div class="col-sm-2"></div>
        </div><!-- row -->

    </div><!-- container -->
</section><!-- section -->

<section class="section galery-area center-text" id="gallery">
    <div class="container">
        <div class="row">

            <div class="col-sm-12">

                <div class="heading">
                    <h2 class="title">Our Gallery</h2>
                    <span class="heading-bottom"><i class="icon icon-star"></i></span>
                </div>

                <div class="image-gallery">
                    <div class="row">
                        @foreach($undangan->images->where('status','1') as $img)
                            <div class="col-md-4 col-sm-6">
                                <a href="{{asset('demos/foto/'.$img->image)}}" data-fluidbox><img class="margin-bottom"
                                                                                                  src="{{asset('demos/foto/'.$img->image)}}"
                                                                                                  alt="Gallery Image"></a>
                            </div>
                    @endforeach
                    <!-- col-sm-4 -->

                        <!-- col-sm-4 -->

                        <div id="x" style="display: none;">
                            <div class="container">
                                <div class="image-gallery">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-6">
                                            <a href="{{asset('demos/images/gallery-4-600x400.jpg')}}" data-fluidbox><img
                                                    class="margin-bottom"
                                                    src="{{asset('demos/images/gallery-4-600x400.jpg')}}"
                                                    alt="Gallery Image"></a>
                                        </div><!-- col-sm-4 -->

                                        <div class="col-md-4 col-sm-6">
                                            <a href="{{asset('demos/images/gallery-5-600x400.jpg')}}" data-fluidbox><img
                                                    class="margin-bottom"
                                                    src="{{asset('demos/images/gallery-5-600x400.jpg')}}"
                                                    alt="Gallery Image"></a>
                                        </div><!-- col-sm-4 -->

                                        <div class="col-md-4 col-sm-6">
                                            <a href="{{asset('demos/images/gallery-1-600x400.jpg')}}" data-fluidbox><img
                                                    class="margin-bottom"
                                                    src="{{asset('demos/images/gallery-1-600x400.jpg')}}"
                                                    alt="Gallery Image"></a>
                                        </div><!-- col-sm-4 -->
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div><!-- row -->

                    <!-- <a class="btn-2 margin-bottom gallery-btn" id="y" onclick="moreLess()" style="display: block;">VIEW ALL GALLERY</a>
                    <a class="btn-2 margin-bottom gallery-btn"  id="z" onclick="moreLess()" style="display: none;">VIEW LESS GALLERY</a> -->
                </div><!-- image-gallery -->

            </div><!-- col-sm-10 -->
        </div><!-- row -->
    </div><!-- container -->
</section><!-- section -->

<section class="contact-area" id="contact">
    <div class="contact-wrapper section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <center>
                        <div class="heading">
                            <h2 class="title">MAPS</h2>
                            <span class="heading-bottom"><i class="icon icon-star"></i></span>
                        </div>
                    </center>
                    <div class="embed-responsive embed-responsive-16by9 margin-bottom">


                        <iframe id="gmap_canvas"
                        src="https://maps.google.com/maps?q={{$undangan->latitude}}, {{$undangan->longitude}}&z=15&output=embed"
                                frameborder="2" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                    </div>
                    <br>
                    <center>
                        <a class="btn-2 margin-bottom gallery-btn" href="https://www.google.com/maps/?q={{$undangan->latitude}},{{$undangan->longitude}}">VIEW ON
                            GOOGLE MAPS</a>
                    </center>
                </div>
            </div>
        </div><!-- container -->
    </div><!-- float-left -->


</section><!-- section -->

<section class="contact-area" id="ucapan">
    <div class="contact-wrapper section">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <center>
                        <div class="heading">
                            <h2 class="title">UCAPAN</h2>
                            <span class="heading-bottom"><i class="icon icon-star"></i></span>
                        </div>
                    </center>

                    <center>
                        <h1><i class="fab fa-whatsapp" style="color: green"></i></h1><br>
                        <a class="btn-2 margin-bottom gallery-btn" href="https://wa.me/{{$undangan->whatsapp_number}}">Klik
                            dan ucapkan
                            Melalui W.A</a>
                    </center>
                </div>
            </div>
        </div><!-- container -->
    </div><!-- float-left -->
    </div>


</section><!-- section -->


<footer>
    <div class="container center-text">
        <div class="logo-wrapper">
            <a class="logo" href="#"><img src="{{asset('demos/images/logo-black.png')}}" alt="Logo Image"></a>
            <i class="icon icon-star"></i>
        </div>
        <h3 class="title">{{$undangan->guy_nickname}} <i style="color: #ff8080"
                                                         class="icon icon-heart"></i> {{$undangan->girl_nickname}}</h3>

        <p class="copyright"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script>document.write(new Date().getFullYear());</script>
            All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a
                href="https://colorlib.com" target="_blank">Colorlib</a>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>

    </div><!-- container -->
</footer>


<!-- SCIPTS -->

<script src="{{asset('demos/common-js/jquery-3.1.1.min.js')}}"></script>

<script src="{{asset('demos/common-js/tether.min.js')}}"></script>

<script src="{{asset('demos/common-js/bootstrap.js')}}"></script>

<script src="{{asset('demos/common-js/jquery.countdown.min.js')}}"></script>

<script src="{{asset('demos/common-js/jquery.fluidbox.min.js')}}"></script>


<script src="{{asset('demos/common-js/scripts.js')}}"></script>
<script type="text/javascript">
    function moreLess() {
        var x = document.getElementById("x");
        var y = document.getElementById("y");
        var z = document.getElementById("z");
        if (x.style.display === "none") {
            x.style.display = "block";
            z.style.display = "block";
            y.style.display = "none";
        } else {
            x.style.display = "none";
            y.style.display = "block";
            z.style.display = "none";
        }
    }
</script>

</body>
</html>
