<!doctype html>
<html class="no-js" lang="zxx" style="scroll-behavior: smooth">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

    <!-- CSS here -->
    <link rel="stylesheet" href="{{asset('customers/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/slicknav.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/animate.min.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/fontawesome-all.min.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/themify-icons.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/nice-select.css')}}">
    <link rel="stylesheet" href="{{asset('customers/css/style.css')}}">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
    crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
   integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
   crossorigin=""></script>
</head>

<body>

<!-- Preloader Start -->
<div id="preloader-active">
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="preloader-inner position-relative">
            <div class="preloader-circle"></div>
            <div class="preloader-img pere-text">
                <img src="{{asset('customers/img/logo/preloader.png')}}" alt="">
            </div>
        </div>
    </div>
</div>
<!-- Preloader Start -->

<header>
    <!-- Header Start -->
    <div class="header-area">
        <div class="main-header header-sticky">
            <div class="container">
                <div class="row align-items-center">
                    <!-- Logo -->
                    <div class="col-xl-2 col-lg-2 col-md-2">
                        <div class="logo">
                            <a href="/"><img src="{{asset('customers/img/logo/logo2.png')}}" height="20" alt=""></a>
                        </div>
                    </div>
                    <div class="col-xl-10 col-lg-10 col-md-10">
                        <!-- Main-menu -->
                        <div class="main-menu f-right d-none d-lg-block">
                            <nav>
                                <ul id="navigation">
                                    <li><a href="/">Beranda</a></li>
                                    <li><a href="#fitur">Fitur</a></li>
                                    <li><a href="/demo">Demo</a></li>
                                    @guest
                                        <li><a href="{{route('customer.login')}}">Masuk / Login</a></li>
    {{--                                    <li><a href="#">Pages</a>--}}
    {{--                                        <ul class="submenu">--}}
    {{--                                            <li><a href="blog.html">Blog</a></li>--}}
    {{--                                            <li><a href="single-blog.html">Blog Details</a></li>--}}
    {{--                                            <li><a href="elements.html">Element</a></li>--}}
    {{--                                        </ul>--}}
    {{--                                    </li>--}}
                                        <li><a href="{{route('customer.register')}}">Daftar</a></li>
                                    @else
                                        <li><a href="{{route('customer.logout')}}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">Logout</a></li>
                                        <form id="logout-form" action="{{ route('customer.logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    @endguest
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>

<main>
    @yield('content')
</main>
<footer>
    <!-- Footer Start-->
    <div class="footer-main footer-bg">
      
        <!-- footer-bottom aera -->
        <div class="footer-bottom-area footer-bg">
            <div class="container">
                <div class="footer-border">
                    <div class="row d-flex align-items-center">
                        <div class="col-xl-12 ">
                            <div class="footer-copy-right text-center">
                                <p>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                                    All rights reserved | This template is made with <i class="ti-heart"
                                                                                        aria-hidden="true"></i> by <a
                                        href="https://colorlib.com" target="_blank">Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->
</footer>

<!-- JS here -->

<!-- All JS Custom Plugins Link Here here -->
<script src="{{asset('customers/js/vendor/modernizr-3.5.0.min.js')}}"></script>
<!-- Jquery, Popper, Bootstrap -->
<script src="{{asset('customers/js/vendor/jquery-1.12.4.min.js')}}"></script>
<script src="{{asset('customers/js/popper.min.js')}}"></script>
<script src="{{asset('customers/js/bootstrap.min.js')}}"></script>

<!-- Jquery Mobile Menu -->
<script src="{{asset('customers/js/jquery.slicknav.min.js')}}"></script>

<!-- Jquery Slick , Owl-Carousel Plugins -->
<script src="{{asset('customers/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('customers/js/slick.min.js')}}"></script>

<!-- Date Picker -->
<script src="{{asset('customers/js/gijgo.min.js')}}"></script>

<!-- One Page, Animated-HeadLin -->
<script src="{{asset('customers/js/wow.min.js')}}"></script>
<script src="{{asset('customers/js/animated.headline.js')}}"></script>
<script src="{{asset('customers/js/jquery.magnific-popup.js')}}"></script>

<!-- Scrollup, nice-select, sticky -->
<script src="{{asset('customers/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('customers/js/jquery.nice-select.min.js')}}"></script>
<script src="{{asset('customers/js/jquery.sticky.js')}}"></script>

<!-- contact js -->
<script src="{{asset('customers/js/contact.js')}}"></script>
<script src="{{asset('customers/js/jquery.form.js')}}"></script>
<script src="{{asset('customers/js/jquery.validate.min.js')}}"></script>
<script src="{{asset('customers/js/mail-script.js')}}"></script>
<script src="{{asset('customers/js/jquery.ajaxchimp.min.js')}}"></script>

<!-- Jquery Plugins, main Jquery -->
<script src="{{asset('customers/js/plugins.js')}}"></script>
<script src="{{asset('customers/js/main.js')}}"></script>
@yield('js')
</body>
</html>
