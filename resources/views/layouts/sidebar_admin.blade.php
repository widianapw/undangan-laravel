<nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
        data-accordion="false">
        <li class="nav-item" class="nav-link">
            <a href="/admin" class="nav-link">
                <i class="nav-icon fas fa-tachometer-alt blue"></i>
                <p>Beranda</p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin.pesanan.index')}}" class="nav-link">
                <i class="nav-icon fas fa-ticket-alt purple"></i>
                <p>
                    Pesanan
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin.agama.index')}}" class="nav-link">
                <i class="nav-icon fa fa-praying-hands orange"></i>
                <p>
                    Agama
                </p>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin.sambutan.index')}}" class="nav-link">
                <i class="nav-icon fab fa-speaker-deck cyan"></i>
                <p>
                    Kata Sambutan
                </p>
            </a>
        </li>
        <hr>
        <li class="nav-item">
            <a href="{{ route('admin.logout') }}"
               onclick="event.preventDefault();
                          document.getElementById('logout-form').submit();" class="nav-link">
                <i class="nav-icon fas fa-power-off red"></i>
                <p>
                    Keluar
                </p>
            </a>
            <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </li>

    </ul>
</nav>
