<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



Route::group(['prefix' => 'cust','as'=>'customer.'], function () {
    Auth::routes(['verify' => true]);
});

Route::group(['as' => 'customer.'], function () {
    Route::get('/', 'CustomerController@index')->name("home");
    
    
    Route::group(['middleware' => 'customer', 'prefix' => 'cust'], function () {
        Route::get('/demo', 'DemoController@index');
        Route::get('/buatundangan', 'CustomerController@register')->name('buatundangan');
        Route::post('/tambahundangan', 'CustomerController@store')->name('tambahundangan');
    });
});


auth::routes();

Route::group(['prefix' => 'admin'], function () {
    Route::get('login', 'AuthAdminController@showLoginForm')->name('admin.login');
    Route::post('login', 'AuthAdminController@login')->name('admin.postlogin');
    Route::post('logout', 'AuthAdminController@logout')->name('admin.logout');
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'as' => 'admin.'], function () {
    Route::resource('/', 'AdminController');
    Route::resource('/agama', 'AgamaController');
    Route::resource('/sambutan', 'SambutanController');
    Route::resource('/pesanan', 'PesananController');
    Route::put('/pesanan/ubahStatus/{id}', 'PesananController@changeStatus')->name('pesanan.changeStatus');
});

// Route::group(['prefix'=>'/admin','middleware'=>'admin'],function(){
//     Route::resource('/','AdminController');
// });

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/{domain}', 'CustomerController@undangan');
